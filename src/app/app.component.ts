import { Component } from '@angular/core';
import { AuthReducerState } from './store/reducers/auth.reducer';
import { Store } from '@ngrx/store';
import * as actions from './store/actions/auth.actions';
import { User } from './models/user';

@Component({
  selector: 'app-root',
  templateUrl: './app.component.html',
  styleUrls: ['./app.component.scss']
})
export class AppComponent {
  constructor(private store: Store<AuthReducerState>) {
    this.initAuth();
  }

  private initAuth(): void {
    const user: User = JSON.parse(localStorage.getItem('user'));

    if (!!user) {
      this.store.dispatch(new actions.SetAuth(user));
    } else {
      this.store.dispatch(new actions.Logout());
    }
  }
}
