import { BrowserModule, } from '@angular/platform-browser';
import { NgModule } from '@angular/core';
import { AppRoutingModule } from './app-routing.module';
import { AuthModule } from './auth/auth.module';
import { BrowserAnimationsModule } from '@angular/platform-browser/animations';
import {
  MatToolbarModule,
  MatIconModule,
  MatButtonModule
} from "@angular/material";
import { InMemoryWebApiModule } from "angular-in-memory-web-api";
import { StoreModule } from '@ngrx/store';
import { EffectsModule } from "@ngrx/effects";
import { StoreDevtoolsModule } from "@ngrx/store-devtools";
import { AuthEffects } from './store/effects/auth.effects';
import { DataService } from './services/data.service';
import { HttpClientModule } from '@angular/common/http';
import { CoursesEffects } from './store/effects/courses.effects';
import { SharedModule } from './shared/shared.module';

import { AppComponent } from './app.component';
import { NotFoundComponent } from './components/not-found/not-found.component';
import { HeaderComponent } from './components/layout/header/header.component';
import { FooterComponent } from './components/layout/footer/footer.component';

import { rootReducer } from './store/reducers';
import { environment } from 'src/environments/environment';

@NgModule({
  declarations: [
    AppComponent,
    NotFoundComponent,
    HeaderComponent,
    FooterComponent
  ],
  imports: [
    BrowserModule,
    BrowserAnimationsModule,
    StoreModule.forRoot(rootReducer()),
    HttpClientModule,
    MatToolbarModule,
    MatIconModule,
    MatButtonModule,
    AuthModule,
    SharedModule,
    AppRoutingModule,
    StoreDevtoolsModule.instrument({ maxAge: 25, logOnly: environment.production }),
    InMemoryWebApiModule.forRoot(DataService),
    EffectsModule.forRoot([AuthEffects, CoursesEffects])
  ],
  providers: [],
  bootstrap: [AppComponent]
})
export class AppModule { }
