import { Component } from '@angular/core';
import { FormBuilder, FormGroup, Validators, AbstractControl } from '@angular/forms';
import { Store, select } from '@ngrx/store';
import * as actions from 'src/app/store/actions';
import { AuthReducerState } from '../../../store/reducers/auth.reducer';
import { Observable } from 'rxjs';
import { map } from "rxjs/operators";

@Component({
  selector: 'app-login',
  templateUrl: './login.component.html',
  styleUrls: ['./login.component.scss']
})
export class LoginComponent {
  form: FormGroup;
  errors$: Observable<any>;

  constructor(private fb: FormBuilder,
    private store: Store<AuthReducerState>) {
    this.buildForm();
    this.errors$ = this.store.pipe(
      select('auth'),
      map((state: AuthReducerState) => state.errors)
    );
  }

  private buildForm(): void {
    this.form = this.fb.group({
      email: [null, [Validators.required, Validators.email]],
      password: [null, Validators.required]
    });
  }

  onSubmit(): void {
    if (this.form.valid) {
      this.store.dispatch(new actions.Login(this.form.value));
    }
  }

  getErrorMessage(formControlName: string): string {
    return this[formControlName].hasError('required') ? 'You must enter a value' :
      this[formControlName].hasError('email') ? 'Not a valid email' :
        '';
  }

  get email(): AbstractControl {
    return this.form.get('email');
  }

  get password(): AbstractControl {
    return this.form.get('password');
  }

}
