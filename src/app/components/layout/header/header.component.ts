import { Component } from '@angular/core';
import { Store, select } from '@ngrx/store';
import { AuthReducerState } from 'src/app/store/reducers/auth.reducer';
import { Observable } from 'rxjs';
import { User } from 'src/app/models/user';
import { map } from "rxjs/operators";
import * as actions from '../../../store/actions';
import { Router } from '@angular/router';

@Component({
  selector: 'app-header',
  templateUrl: './header.component.html',
  styleUrls: ['./header.component.scss']
})
export class HeaderComponent {
  user$: Observable<User>;
  isAuthenticated$: Observable<boolean>;

  constructor(private store: Store<AuthReducerState>,
    private router: Router) {
    this.user$ = this.store.pipe(
      select('auth'),
      map((state: AuthReducerState) => state.user)
    );
    this.isAuthenticated$ = this.store.pipe(
      select('auth'),
      map((state: AuthReducerState) => state.isAuthenticated)
    );
  }

  onLogOff(): void {
    this.store.dispatch(new actions.Logout());
    this.router.navigate(['/']);
  }
}
