import { Component, OnInit } from '@angular/core';
import { FormGroup, FormBuilder, Validators, AbstractControl } from '@angular/forms';
import { Router, ActivatedRoute } from '@angular/router';
import { Store, select } from '@ngrx/store';
import { CoursesState } from 'src/app/store/reducers/courses.reducer';
import * as actions from '../../../store/actions';
import { Observable } from 'rxjs';
import { Course } from 'src/app/models/course';
import { map } from 'rxjs/operators';

@Component({
  selector: 'app-add-edit-course',
  templateUrl: './add-edit-course.component.html',
  styleUrls: ['./add-edit-course.component.scss']
})
export class AddEditCourseComponent implements OnInit {
  form: FormGroup;
  course$: Observable<Course>;
  private editing: boolean;
  private courseId: number;

  constructor(private fb: FormBuilder,
    private router: Router,
    private activatedRoute: ActivatedRoute,
    private store: Store<CoursesState>) {
    this.courseId = parseInt(activatedRoute.snapshot.paramMap.get('id'));
    this.editing = !!this.courseId;

    if (this.editing) {
      this.course$ = store
        .pipe(
          select('courses'),
          map((state: CoursesState) => state.course)
        );
    }

    this.buildForm();
  }

  ngOnInit() {
    if (this.editing) {
      this.store.dispatch(new actions.GetCourse(this.courseId));
    }
  }

  private buildForm(): void {
    this.form = this.fb.group({
      title: [null, Validators.required],
      description: [null, Validators.required],
      date: [null, Validators.required],
      duration: [null, Validators.required],
      authors: [null, Validators.required]
    });
  }

  onSubmit(): void {
    if (this.form.valid) {
      if (!this.editing) {
        this.store.dispatch(new actions.CreateCourse(this.form.value))
      }
    }
  }

  onCancel(): void {
    this.router.navigate(['/courses']);
  }

  getErrorMessage(controlName: string): string {
    return `${this.ucfirst(controlName)} is required.`;
  }

  private ucfirst(string: string): string {
    return string.charAt(0).toUpperCase() + string.slice(1);
  }

  get title(): AbstractControl {
    return this.form.get('title');
  }

  get description(): AbstractControl {
    return this.form.get('description');
  }

  get date(): AbstractControl {
    return this.form.get('date');
  }

  get duration(): AbstractControl {
    return this.form.get('duration');
  }

  get authors(): AbstractControl {
    return this.form.get('authors');
  }
}
