import { Component, OnInit, Input } from '@angular/core';
import { Course } from 'src/app/models/course';
import * as moment from "moment";
import { Store } from '@ngrx/store';
import { CoursesState } from 'src/app/store/reducers/courses.reducer';
import * as actions from '../../../store/actions';

@Component({
  selector: 'app-course',
  templateUrl: './course.component.html',
  styleUrls: ['./course.component.scss']
})
export class CourseComponent implements OnInit {
  @Input('course')
  course: Course;

  constructor(private store: Store<CoursesState>) { }

  ngOnInit() {
  }

  getDuration(durationMins: number): string {
    const duration = moment.duration({
      minutes: durationMins
    });

    return duration.humanize();
  }

  getCourseDate(date: string): string {
    return moment(new Date(date)).format('MM.DD.YYYY');
  }

  onDelete(id: number): void {
    this.store.dispatch(new actions.DeleteCourse(id));
  }
}
