import { Component, OnInit } from '@angular/core';
import { Store, select } from '@ngrx/store';
import { CoursesState } from 'src/app/store/reducers/courses.reducer';
import * as actions from '../../../store/actions';
import { Observable } from 'rxjs';
import { Course } from 'src/app/models/course';
import { map } from "rxjs/operators";
import { CoursesAPIParams } from 'src/app/services/courses.service';

@Component({
  selector: 'app-courses',
  templateUrl: './courses.component.html',
  styleUrls: ['./courses.component.scss']
})
export class CoursesComponent implements OnInit {
  courses$: Observable<Course[]>;
  loaded: number;
  count: number;
  searchText: string;

  constructor(private store: Store<CoursesState>) {
    this.courses$ = store.pipe(
      select('courses'),
      map((state: CoursesState) => state.courses)
    );
    this.store
      .select('courses')
      .subscribe((state: any) => {
        this.loaded = state.loaded;
        this.count = state.count;
      });
  }

  ngOnInit() {
    if (this.loaded === 0) {
      this.store.dispatch(new actions.GetCourses());
    }
  }

  loadMore(): void {
    const params: CoursesAPIParams = {
      loaded: this.loaded,
      count: this.count
    };

    this.store.dispatch(new actions.GetCourses(params));
  }
}
