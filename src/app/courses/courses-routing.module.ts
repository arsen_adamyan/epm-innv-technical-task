import { NgModule } from '@angular/core';
import { Routes, RouterModule } from "@angular/router";
import { CoursesComponent } from './components/courses/courses.component';
import { AddEditCourseComponent } from './components/add-edit-course/add-edit-course.component';

const routes: Routes = [
    {
        path: '',
        component: CoursesComponent
    },
    {
        path: 'new',
        component: AddEditCourseComponent
    },
    {
        path: ':id',
        component: AddEditCourseComponent
    }
];

@NgModule({
    imports: [RouterModule.forChild(routes)],
    exports: [RouterModule]
})
export class CoursesRoutingModule { }