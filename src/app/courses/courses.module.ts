import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { CoursesRoutingModule } from './courses-routing.module';
import {
  MatDividerModule,
  MatFormFieldModule,
  MatInputModule,
  MatButtonModule
} from "@angular/material";
import { ReactiveFormsModule, FormsModule } from '@angular/forms';
import { SharedModule } from '../shared/shared.module';
import { NgxMaskModule } from 'ngx-mask'

import { CoursesComponent } from './components/courses/courses.component';
import { AddEditCourseComponent } from './components/add-edit-course/add-edit-course.component';
import { CourseComponent } from './components/course/course.component';

@NgModule({
  declarations: [
    CoursesComponent,
    AddEditCourseComponent,
    CourseComponent
  ],
  imports: [
    CommonModule,
    FormsModule,
    SharedModule,
    CoursesRoutingModule,
    MatDividerModule,
    MatFormFieldModule,
    MatInputModule,
    MatButtonModule,
    ReactiveFormsModule,
    NgxMaskModule.forRoot()
  ]
})
export class CoursesModule { }
