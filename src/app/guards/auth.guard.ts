import { Injectable } from '@angular/core';
import { CanActivate, Router } from '@angular/router';
import { Observable } from 'rxjs';
import { Store } from '@ngrx/store';
import { AuthReducerState } from '../store/reducers/auth.reducer';
import { map, tap } from "rxjs/operators";

@Injectable({
  providedIn: 'root'
})
export class AuthGuard implements CanActivate {
  constructor(private store: Store<AuthReducerState>,
    private router: Router) { }

  canActivate(): Observable<boolean> | Promise<boolean> | boolean {
    return this.store
      .select('auth')
      .pipe(
        map((auth: AuthReducerState) => auth.isAuthenticated),
        tap((isAuthenticated: boolean) => {
          if (!isAuthenticated) {
            this.router.navigate(['/login']);
          }
        })
      )
  }
}
