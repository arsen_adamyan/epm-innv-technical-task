import { Injectable } from '@angular/core';
import { CanActivate, Router } from '@angular/router';
import { Observable } from 'rxjs';
import { Store } from '@ngrx/store';
import { AuthReducerState } from '../store/reducers/auth.reducer';
import { map, tap } from "rxjs/operators";

@Injectable({
  providedIn: 'root'
})
export class NonAuthGuard implements CanActivate {
  constructor(private router: Router,
    private store: Store<AuthReducerState>) { }

  canActivate(): Observable<boolean> | Promise<boolean> | boolean {
    return this.store
      .select('auth')
      .pipe(
        map((auth: AuthReducerState) => {
          if (auth.isAuthenticated) {
            this.router.navigate(['/courses']);
            return false;
          } else {
            return true;
          }
        }),
      );
  }
}
