import { Pipe, PipeTransform } from '@angular/core';
import { Course } from '../models/course';

@Pipe({
  name: 'search'
})
export class SearchPipe implements PipeTransform {

  transform(value: Course[], searchText: string): any {
    if (!value) {
      return [];
    }

    if (!searchText) {
      return value;
    }

    searchText = searchText.trim().toLowerCase();

    return value.filter((course: Course) => {
      return course.title.toLowerCase().includes(searchText);
    });
  }

}
