import { Injectable } from '@angular/core';
import { Observable } from 'rxjs';
import { User } from '../models/user';

export interface loginAPIParams {
  email: string;
  password: string;
};

@Injectable({
  providedIn: 'root'
})
export class AuthService {
  private readonly TOKEN = 'eyJhbGciOiJIUzI1NiIsInR5cCI6IkpXVCJ9.eyJzdWIiOiIxMjM0NTY3ODkwIiwibmFtZSI6IkpvaG4gRG9lIiwiaWF0IjoxNTE2MjM5MDIyfQ.SflKxwRJSMeKKF2QT4fwpMeJf36POk6yJV_adQssw5c';

  login(credentials: loginAPIParams): Observable<User> {
    return new Observable(observer => {
      if (this.checkAuth(credentials)) {
        observer.next({ username: 'John', token: this.TOKEN });
      } else {
        observer.error({ success: false });
      }
    })
  }

  private checkAuth(credentials: loginAPIParams): boolean {
    const { email, password } = credentials;

    return email === 'user@mail.com' && password === 'password';
  }
}
