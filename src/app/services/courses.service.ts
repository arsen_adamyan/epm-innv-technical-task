import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';
import { Observable } from 'rxjs';
import { Course } from '../models/course';
import { map } from "rxjs/operators";
import { CoursesState } from '../store/reducers/courses.reducer';

export interface CoursesAPIParams {
  count: number;
  loaded: number;
}

@Injectable({
  providedIn: 'root'
})
export class CoursesService {
  private readonly API_URL = '/api';

  constructor(private http: HttpClient) { }

  getCourses(params?: CoursesAPIParams): Observable<CoursesState> {
    return this.http
      .get(`${this.API_URL}/courses`)
      .pipe(
        map((allCourses: Course[]) => {
          if (!params) {
            const courses = allCourses.slice(0, 5);

            return {
              courses,
              loaded: courses.length,
              count: allCourses.length,
              course: null
            };
          } else {
            const { loaded, count } = params;

            return {
              count,
              loaded: loaded + 5,
              courses: allCourses.splice(loaded, 5),
              course: null
            };
          }
        })
      );
  }

  delete(id: number): Observable<number> {
    return this.http
      .delete(`${this.API_URL}/courses/${id}`)
      .pipe(
        map((response: any) => id)
      );
  }

  get(id: number): Observable<Course> {
    return this.http
      .get(`${this.API_URL}/courses/${id}`)
      .pipe(
        map((course: Course) => course)
      );
  }

  create(course: Course): Observable<Course> {
    return this.http
      .post(`${this.API_URL}/courses`, course)
      .pipe(
        map((course: Course) => course)
      );
  }
}
