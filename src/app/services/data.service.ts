import { Injectable } from '@angular/core';
import { InMemoryDbService } from 'angular-in-memory-web-api'

@Injectable({
  providedIn: 'root'
})
export class DataService implements InMemoryDbService {
  createDb() {
    const courses = [];

    for (let i = 1; i <= 20; i++) {
      const minDuration = 5;
      const maxDuration = 60;
      const course = {
        id: i,
        title: `Video Course ${i}`,
        duration: Math.floor(Math.random() * (maxDuration - minDuration) + minDuration),
        date: new Date(),
        description: "It is a long established fact that a reader will be distracted by the readable content of a page when looking at its layout. The point of using Lorem Ipsum is that it has a more-or-less normal distribution of letters, as opposed to using 'Content here, content here', making it look like readable English. Many desktop publishing packages and web page editors now use Lorem Ipsum as their default model text, and a search for 'lorem ipsum' will uncover many web sites still in their infancy. Various versions have evolved over the years, sometimes by accident, sometimes on purpose (injected humour and the like)."
      };

      courses.push(course);
    }

    return { courses };
  }
}
