import { Action } from "@ngrx/store";
import { ActionTypes } from '../action.types';
import { loginAPIParams } from 'src/app/services/auth.service';
import { User } from 'src/app/models/user';

export class Login implements Action {
    readonly type = ActionTypes.LOGIN;

    constructor(public payload: loginAPIParams) {
    }
}

export class SetAuth implements Action {
    readonly type = ActionTypes.SET_AUTH;

    constructor(public payload: User) {
        localStorage.setItem('user', JSON.stringify(payload));
    }
}

export class SetErrors implements Action {
    readonly type = ActionTypes.SET_ERRORS;

    constructor(public payload: any) {

    }
}

export class Logout implements Action {
    readonly type = ActionTypes.LOGOUT;

    constructor() {
        localStorage.removeItem('user');
    }
}