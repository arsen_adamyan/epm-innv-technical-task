import { Action } from '@ngrx/store';
import { ActionTypes } from '../action.types';
import { CoursesAPIParams } from 'src/app/services/courses.service';
import { CoursesState } from '../reducers/courses.reducer';
import { Course } from 'src/app/models/course';

export class GetCourses implements Action {
    readonly type = ActionTypes.GET_COURSES;

    constructor(public payload?: CoursesAPIParams) { }
}

export class GetCourse implements Action {
    readonly type = ActionTypes.GET_COURSE;

    constructor(public payload: number) { }
}

export class SetCourse implements Action {
    readonly type = ActionTypes.SET_COURSE;

    constructor(public payload: Course) { }
}

export class SetCourses implements Action {
    readonly type = ActionTypes.SET_COURSES;

    constructor(public payload: CoursesState) { }
}

export class DeleteCourse implements Action {
    readonly type = ActionTypes.DELETE_COURSE;

    constructor(public payload: number) { }
}

export class UnsetCourse implements Action {
    readonly type = ActionTypes.UNSET_COURSE;

    constructor(public payload: number) { }
}

export class CreateCourse implements Action {
    readonly type = ActionTypes.CREATE_COURSE;

    constructor(public payload: Course) { }
}

export class AddCourse implements Action {
    readonly type = ActionTypes.ADD_COURSE;

    constructor(public payload: Course) { }
}