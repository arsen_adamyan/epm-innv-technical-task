import { Injectable } from "@angular/core";
import { Effect, Actions, ofType } from "@ngrx/effects";
import { ActionTypes } from '../action.types';
import { switchMap, map, catchError, tap } from "rxjs/operators";
import { AuthService } from 'src/app/services/auth.service';
import { Router } from '@angular/router';
import * as actions from '../actions';
import { of } from 'rxjs';
import { User } from 'src/app/models/user';

@Injectable()
export class AuthEffects {
    constructor(private actions$: Actions,
        private authService: AuthService,
        private router: Router) { }

    @Effect()
    login$ = this.actions$
        .pipe(
            ofType(ActionTypes.LOGIN),
            switchMap((action: any) => this.authService
                .login(action.payload)
                .pipe(
                    map((user: User) => new actions.SetAuth(user)),
                    tap(() => this.router.navigate(['/courses'])),
                    catchError((error: any) => of(new actions.SetErrors(error)))
                ))
        );
}