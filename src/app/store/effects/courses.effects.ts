import { Injectable } from "@angular/core";
import { Actions, Effect, ofType } from '@ngrx/effects';
import { CoursesService } from 'src/app/services/courses.service';
import { ActionTypes } from '../action.types';
import { switchMap, map, catchError, tap } from 'rxjs/operators';
import * as actions from '../actions';
import { CoursesState } from '../reducers/courses.reducer';
import { Course } from 'src/app/models/course';
import { EMPTY } from 'rxjs';
import { Router } from '@angular/router';

@Injectable()
export class CoursesEffects {
    constructor(private actions$: Actions,
        private coursesService: CoursesService,
        private router: Router) { }

    @Effect()
    getCourses$ = this.actions$
        .pipe(
            ofType(ActionTypes.GET_COURSES),
            switchMap((action: any) => this.coursesService
                .getCourses(action.payload)
                .pipe(
                    map((coursesState: CoursesState) => new actions.SetCourses(coursesState))
                )
            )
        );

    @Effect()
    deleteCourse = this.actions$
        .pipe(
            ofType(ActionTypes.DELETE_COURSE),
            switchMap((action: any) => this.coursesService
                .delete(action.payload)
                .pipe(
                    map((id: number) => new actions.UnsetCourse(id))
                )
            )
        );

    @Effect()
    getCourse = this.actions$
        .pipe(
            ofType(ActionTypes.GET_COURSE),
            switchMap((action: any) => this.coursesService
                .get(action.payload)
                .pipe(
                    map((course: Course) => new actions.SetCourse(course)),
                    catchError((error: any) => {
                        if (error.status === 404) {
                            this.router.navigate(['/notfound']);
                        }

                        return EMPTY;
                    })
                )
            )
        );

    @Effect()
    createCourse = this.actions$
        .pipe(
            ofType(ActionTypes.CREATE_COURSE),
            switchMap((action: any) => this.coursesService
                .create(action.payload)
                .pipe(
                    map((course: Course) => new actions.AddCourse(course)),
                    tap(() => this.router.navigate(['/courses']))
                )
            )
        );
}