import { ActionTypes } from "../action.types";
import { User } from "../../models/user";

export interface AuthReducerState {
    isAuthenticated: boolean;
    user: null | User;
    errors: any;
};

export const initialState: AuthReducerState = {
    isAuthenticated: false,
    user: null,
    errors: null
};

export function authReducer(state: AuthReducerState = initialState, action): AuthReducerState {
    switch (action.type) {
        case ActionTypes.SET_AUTH:
            return {
                ...state,
                isAuthenticated: true,
                user: action.payload
            };
        case ActionTypes.SET_ERRORS:
            return {
                ...state,
                errors: { message: 'Wrong email or password' }
            }
        case ActionTypes.LOGOUT:
            return {
                ...state,
                isAuthenticated: false,
                user: null,
                errors: null
            };
        default:
            return state;
    }
}