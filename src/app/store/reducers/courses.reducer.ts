import { Course } from 'src/app/models/course';
import { ActionTypes } from '../action.types';

export interface CoursesState {
    courses: Course[];
    loaded: number;
    count: number;
    course: Course | null
}

export const initialState: CoursesState = {
    courses: [],
    loaded: 0,
    count: 0,
    course: null
};

export function coursesReducer(state: CoursesState = initialState, action): CoursesState {
    switch (action.type) {
        case ActionTypes.SET_COURSES:
            const { courses, loaded, count } = action.payload;

            return {
                ...state,
                loaded,
                count,
                courses: state.courses.concat(courses)
            };
        case ActionTypes.UNSET_COURSE:
            return {
                ...state,
                loaded: state.loaded - 1,
                courses: state.courses.filter((course: Course) => course.id !== action.payload)
            };
        case ActionTypes.SET_COURSE:
            return {
                ...state,
                course: action.payload
            };
        case ActionTypes.ADD_COURSE:
            return {
                ...state,
                count: state.count + 1,
                loaded: state.loaded + 1,
                courses: state.count === state.loaded ? state.courses.concat({ ...action.payload, date: new Date() }) : state.courses
            };
        default:
            return state;
    }
}