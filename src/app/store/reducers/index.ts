import { authReducer as auth } from './auth.reducer';
import { coursesReducer as courses } from './courses.reducer';

export function rootReducer() {
    return {
        auth,
        courses
    };
}